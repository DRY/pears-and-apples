package com.example.pearsandapples.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "providers")
public class Provider {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;

}
