package com.example.pearsandapples.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "delivery_details")
public class DeliveryDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @ManyToOne
    @JoinColumn(name = "delivery_id")
    private Delivery delivery;
    @ManyToOne
    @JoinColumn(name = "price_id")
    private Price price;
    private Double weight;

    public DeliveryDetail(Delivery delivery, Price price, Double weight) {
        this.delivery = delivery;
        this.price = price;
        this.weight = weight;
    }

    public DeliveryDetail() {
    }
}
