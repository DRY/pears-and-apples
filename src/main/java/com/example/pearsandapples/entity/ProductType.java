package com.example.pearsandapples.entity;

import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "product_types")
public class ProductType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String name;
}
