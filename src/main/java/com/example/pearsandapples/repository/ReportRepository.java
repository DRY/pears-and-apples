package com.example.pearsandapples.repository;

import com.example.pearsandapples.util.HibernateUtil;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class ReportRepository {

    public List<Object[]> getReportByDates(String dateFrom, String dateTo) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            final Query query = session.createSQLQuery(
                    "select prod.NAME as PRODUCT_NAME, p.NAME, sum(dd.WEIGHT), sum(dd.WEIGHT)*pr.PRICE " +
                    "from PROVIDERS p, PRICIES pr, DELIVERIES d, DELIVERY_DETAILS dd, PRODUCTS prod " +
                    "where p.ID = d.PROVIDER_ID and d.ID = dd.DELIVERY_ID and dd.PRICE_ID = pr.ID " +
                    "and prod.ID = pr.PRODUCT_ID " +
                    "and d.DATE between PARSEDATETIME('" + dateFrom + "','yyyy-MM-dd') and PARSEDATETIME('" + dateTo + "','yyyy-MM-dd') " +
                    "group by prod.NAME, p.NAME");
            return query.getResultList();
        }
    }
}
