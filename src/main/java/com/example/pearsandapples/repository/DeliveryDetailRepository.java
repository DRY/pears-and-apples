package com.example.pearsandapples.repository;

import com.example.pearsandapples.entity.DeliveryDetail;
import com.example.pearsandapples.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

@Repository
public class DeliveryDetailRepository {

    public void save(DeliveryDetail deliveryDetail) {
        if (deliveryDetail.getWeight() > 0.0) {
            Transaction transaction = null;
            try (Session session = HibernateUtil.getSessionFactory().openSession()) {
                transaction = session.beginTransaction();
                session.save(deliveryDetail);
                transaction.commit();
            } catch (Exception e) {
                if (transaction != null) {
                    transaction.rollback();
                }
                e.printStackTrace();
            }
        }
    }
}
