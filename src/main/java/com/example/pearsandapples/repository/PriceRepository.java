package com.example.pearsandapples.repository;

import com.example.pearsandapples.entity.Price;
import com.example.pearsandapples.entity.Provider;
import com.example.pearsandapples.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Repository
public class PriceRepository {

    public void save(Price price) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(price);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public List<Price> getCurrentPriceByProvider(Provider provider) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            final CriteriaQuery<Price> query = builder.createQuery(Price.class);
            final Root<Price> root = query.from(Price.class);
            Date dateNow = new Date();
            List<Predicate> predicates = new ArrayList<>();
            predicates.add(builder.equal(root.get("provider"), provider.getId()));
            predicates.add(builder.greaterThanOrEqualTo(root.<Date>get("dateTo").as(java.sql.Date.class),
                    dateNow));
            predicates.add(builder.lessThanOrEqualTo(root.<Date>get("dateFrom").as(java.sql.Date.class),
                    dateNow));
            query.select(root).where(predicates.toArray(new Predicate[]{}));
            return session.createQuery(query).list();
        }
    }

    public Price getPriceById(Long id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            final CriteriaQuery<Price> query = builder.createQuery(Price.class);
            final Root<Price> root = query.from(Price.class);
            query.select(root).where(builder.equal(root.get("id"), id));
            return session.createQuery(query).uniqueResult();
        }
    }
}
