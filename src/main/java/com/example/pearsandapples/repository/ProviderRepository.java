package com.example.pearsandapples.repository;

import com.example.pearsandapples.entity.Provider;
import com.example.pearsandapples.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class ProviderRepository {

    public void save(Provider provider) {
        Transaction transaction = null;
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            transaction = session.beginTransaction();
            session.save(provider);
            transaction.commit();
        } catch (Exception e) {
            if (transaction != null) {
                transaction.rollback();
            }
            e.printStackTrace();
        }
    }

    public List<Provider> getProviders() {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            final CriteriaQuery<Provider> query = builder.createQuery(Provider.class);
            query.from(Provider.class);
            return session.createQuery(query).list();
        }
    }

    public Provider getProviderById(Long id) {
        try (Session session = HibernateUtil.getSessionFactory().openSession()) {
            CriteriaBuilder builder = session.getCriteriaBuilder();
            final CriteriaQuery<Provider> query = builder.createQuery(Provider.class);
            final Root<Provider> root = query.from(Provider.class);
            query.select(root).where(builder.equal(root.get("id"), id));
            return session.createQuery(query).uniqueResult();
        }
    }
}
