package com.example.pearsandapples.controller;

import com.example.pearsandapples.entity.Provider;
import com.example.pearsandapples.repository.ProviderRepository;
import com.example.pearsandapples.repository.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
public class HomeController {

    private final ProviderRepository providerRepository;
    private final ReportRepository reportRepository;

    @Autowired
    public HomeController(ProviderRepository providerRepository, ReportRepository reportRepository) {
        this.providerRepository = providerRepository;
        this.reportRepository = reportRepository;
    }

    @GetMapping("")
    public String viewEmpty() {
        return "redirect:/";
    }

    @GetMapping("/")
    public String viewSlash(Map<String, Object> model) {
        List<Provider> providerList = providerRepository.getProviders();
        String curDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        model.put("curDate", curDate);
        model.put("providerList", providerList);
        return "home";
    }

    @GetMapping("/home")
    public String home() {
        return "redirect:/";
    }

    @PostMapping("/report")
    public String getReport(@ModelAttribute("dateFrom") String dateFrom,
                            @ModelAttribute("dateTo") String dateTo, Map<String, Object> model) {
        System.out.println("dateFrom: " + dateFrom);
        System.out.println("dateTo: " + dateTo);
        List<Object[]> reportProductsList = reportRepository.getReportByDates(dateFrom, dateTo);
        model.put("reportProductsList", reportProductsList);
        model.put("dateTo", dateTo);
        model.put("dateFrom", dateFrom);
        return "report";
    }
}
