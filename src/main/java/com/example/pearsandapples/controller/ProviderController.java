package com.example.pearsandapples.controller;

import com.example.pearsandapples.entity.Delivery;
import com.example.pearsandapples.entity.DeliveryDetail;
import com.example.pearsandapples.entity.Price;
import com.example.pearsandapples.entity.Provider;
import com.example.pearsandapples.model.DeliveryForm;
import com.example.pearsandapples.repository.DeliveryDetailRepository;
import com.example.pearsandapples.repository.DeliveryRepository;
import com.example.pearsandapples.repository.PriceRepository;
import com.example.pearsandapples.repository.ProviderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/provider")
public class ProviderController {

    private final ProviderRepository providerRepository;
    private final PriceRepository priceRepository;
    private final DeliveryRepository deliveryRepository;
    private final DeliveryDetailRepository deliveryDetailRepository;

    @Autowired
    public ProviderController(ProviderRepository providerRepository, PriceRepository priceRepository,
                              DeliveryRepository deliveryRepository, DeliveryDetailRepository deliveryDetailRepository) {
        this.providerRepository = providerRepository;
        this.priceRepository = priceRepository;
        this.deliveryRepository = deliveryRepository;
        this.deliveryDetailRepository = deliveryDetailRepository;
    }

    @GetMapping("/{id}")
    public String addDelivery(@PathVariable Long id, Map<String, Object> model) {
        Provider provider = providerRepository.getProviderById(id);
        List<Price> priceList = priceRepository.getCurrentPriceByProvider(provider);

        DeliveryForm deliveryForm = new DeliveryForm();
        deliveryForm.setProviderId(provider.getId());
        deliveryForm.setPriceId1(priceList.get(0).getId());
        deliveryForm.setPriceId2(priceList.get(1).getId());
        deliveryForm.setPriceId3(priceList.get(2).getId());
        deliveryForm.setPriceId4(priceList.get(3).getId());
        deliveryForm.setWeight1(0.0);
        deliveryForm.setWeight2(0.0);
        deliveryForm.setWeight3(0.0);
        deliveryForm.setWeight4(0.0);

        String curDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        model.put("curDate", curDate);

        model.put("provider", provider);
        model.put("price0", priceList.get(0));
        model.put("price1", priceList.get(1));
        model.put("price2", priceList.get(2));
        model.put("price3", priceList.get(3));
        model.put("deliveryForm", deliveryForm);
        return "provider";
    }

    @PostMapping("/delivery")
    public String addDelivery(DeliveryForm deliveryForm, @ModelAttribute("dateTo") String dateTo) throws ParseException {
        System.out.println("dateTo: " + dateTo);
        Delivery delivery = new Delivery();
        delivery.setProvider(providerRepository.getProviderById(deliveryForm.getProviderId()));
        delivery.setDate(new SimpleDateFormat("yyyy-MM-dd").parse(dateTo));
        deliveryRepository.save(delivery);
        DeliveryDetail deliveryDetail1 = new DeliveryDetail(delivery,
                priceRepository.getPriceById(deliveryForm.getPriceId1()), deliveryForm.getWeight1());
        deliveryDetailRepository.save(deliveryDetail1);
        DeliveryDetail deliveryDetail2 = new DeliveryDetail(delivery,
                priceRepository.getPriceById(deliveryForm.getPriceId2()), deliveryForm.getWeight2());
        deliveryDetailRepository.save(deliveryDetail2);
        DeliveryDetail deliveryDetail3 = new DeliveryDetail(delivery,
                priceRepository.getPriceById(deliveryForm.getPriceId3()), deliveryForm.getWeight3());
        deliveryDetailRepository.save(deliveryDetail3);
        DeliveryDetail deliveryDetail4 = new DeliveryDetail(delivery,
                priceRepository.getPriceById(deliveryForm.getPriceId4()), deliveryForm.getWeight4());
        deliveryDetailRepository.save(deliveryDetail4);
        return "redirect:/home";
    }
}
