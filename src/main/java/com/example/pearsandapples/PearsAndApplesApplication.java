package com.example.pearsandapples;

import com.example.pearsandapples.entity.Price;
import com.example.pearsandapples.entity.Product;
import com.example.pearsandapples.entity.ProductType;
import com.example.pearsandapples.entity.Provider;
import com.example.pearsandapples.repository.PriceRepository;
import com.example.pearsandapples.repository.ProductRepository;
import com.example.pearsandapples.repository.ProductTypeRepository;
import com.example.pearsandapples.repository.ProviderRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.text.SimpleDateFormat;

@SpringBootApplication
public class PearsAndApplesApplication {

    public static void main(String[] args) {
        SpringApplication.run(PearsAndApplesApplication.class, args);
    }

    @Bean
    CommandLineRunner runner(ProductTypeRepository productTypeRepository, ProviderRepository providerRepository,
                             ProductRepository productRepository, PriceRepository priceRepository) {
        return args -> {
            ProductType productTypePear = new ProductType();
            productTypePear.setName("Груша");
            productTypeRepository.save(productTypePear);
            ProductType productTypeApple = new ProductType();
            productTypeApple.setName("Яблоко");
            productTypeRepository.save(productTypeApple);

            Provider provider1 = new Provider();
            provider1.setName("Поставщик1");
            providerRepository.save(provider1);
            Provider provider2 = new Provider();
            provider2.setName("Поставщик2");
            providerRepository.save(provider2);
            Provider provider3 = new Provider();
            provider3.setName("Поставщик3");
            providerRepository.save(provider3);

            Product product1 = new Product();
            product1.setName("Лимонка");
            product1.setProductType(productTypePear);
            productRepository.save(product1);
            Product product2 = new Product();
            product2.setName("Лада");
            product2.setProductType(productTypePear);
            productRepository.save(product2);
            Product product3 = new Product();
            product3.setName("Скороспелка");
            product3.setProductType(productTypePear);
            productRepository.save(product3);

            Product product4 = new Product();
            product4.setName("Антоновка");
            product4.setProductType(productTypeApple);
            productRepository.save(product4);
            Product product5 = new Product();
            product5.setName("Айдаред");
            product5.setProductType(productTypeApple);
            productRepository.save(product5);
            Product product6 = new Product();
            product6.setName("Бреберн");
            product6.setProductType(productTypeApple);
            productRepository.save(product6);

            Price price1 = new Price();
            price1.setProvider(provider1);
            price1.setProduct(product1);
            price1.setPrice(10.0);
            price1.setDateFrom(new SimpleDateFormat("yyyy-MM-dd").parse("2021-06-01"));
            price1.setDateTo(new SimpleDateFormat("yyyy-MM-dd").parse("2021-08-10"));
            priceRepository.save(price1);
            Price price2 = new Price();
            price2.setProvider(provider1);
            price2.setProduct(product3);
            price2.setPrice(20.0);
            price2.setDateFrom(new SimpleDateFormat("yyyy-MM-dd").parse("2021-06-01"));
            price2.setDateTo(new SimpleDateFormat("yyyy-MM-dd").parse("2021-08-10"));
            priceRepository.save(price2);
            Price price3 = new Price();
            price3.setProvider(provider1);
            price3.setProduct(product4);
            price3.setPrice(8.0);
            price3.setDateFrom(new SimpleDateFormat("yyyy-MM-dd").parse("2021-06-01"));
            price3.setDateTo(new SimpleDateFormat("yyyy-MM-dd").parse("2021-08-10"));
            priceRepository.save(price3);
            Price price4 = new Price();
            price4.setProvider(provider1);
            price4.setProduct(product5);
            price4.setPrice(12.0);
            price4.setDateFrom(new SimpleDateFormat("yyyy-MM-dd").parse("2021-06-01"));
            price4.setDateTo(new SimpleDateFormat("yyyy-MM-dd").parse("2021-08-10"));
            priceRepository.save(price4);

            Price price5 = new Price();
            price5.setProvider(provider2);
            price5.setProduct(product1);
            price5.setPrice(11.0);
            price5.setDateFrom(new SimpleDateFormat("yyyy-MM-dd").parse("2021-06-02"));
            price5.setDateTo(new SimpleDateFormat("yyyy-MM-dd").parse("2021-08-12"));
            priceRepository.save(price5);
            Price price6 = new Price();
            price6.setProvider(provider2);
            price6.setProduct(product2);
            price6.setPrice(21.0);
            price6.setDateFrom(new SimpleDateFormat("yyyy-MM-dd").parse("2021-06-02"));
            price6.setDateTo(new SimpleDateFormat("yyyy-MM-dd").parse("2021-08-12"));
            priceRepository.save(price6);
            Price price7 = new Price();
            price7.setProvider(provider2);
            price7.setProduct(product4);
            price7.setPrice(9.0);
            price7.setDateFrom(new SimpleDateFormat("yyyy-MM-dd").parse("2021-06-02"));
            price7.setDateTo(new SimpleDateFormat("yyyy-MM-dd").parse("2021-08-12"));
            priceRepository.save(price7);
            Price price8 = new Price();
            price8.setProvider(provider2);
            price8.setProduct(product6);
            price8.setPrice(32.0);
            price8.setDateFrom(new SimpleDateFormat("yyyy-MM-dd").parse("2021-06-02"));
            price8.setDateTo(new SimpleDateFormat("yyyy-MM-dd").parse("2021-08-12"));
            priceRepository.save(price8);

            Price price9 = new Price();
            price9.setProvider(provider3);
            price9.setProduct(product2);
            price9.setPrice(18.0);
            price9.setDateFrom(new SimpleDateFormat("yyyy-MM-dd").parse("2021-06-03"));
            price9.setDateTo(new SimpleDateFormat("yyyy-MM-dd").parse("2021-08-13"));
            priceRepository.save(price9);
            Price price10 = new Price();
            price10.setProvider(provider3);
            price10.setProduct(product3);
            price10.setPrice(28.0);
            price10.setDateFrom(new SimpleDateFormat("yyyy-MM-dd").parse("2021-06-03"));
            price10.setDateTo(new SimpleDateFormat("yyyy-MM-dd").parse("2021-08-13"));
            priceRepository.save(price10);
            Price price11 = new Price();
            price11.setProvider(provider3);
            price11.setProduct(product5);
            price11.setPrice(5.0);
            price11.setDateFrom(new SimpleDateFormat("yyyy-MM-dd").parse("2021-06-03"));
            price11.setDateTo(new SimpleDateFormat("yyyy-MM-dd").parse("2021-08-13"));
            priceRepository.save(price11);
            Price price12 = new Price();
            price12.setProvider(provider3);
            price12.setProduct(product6);
            price12.setPrice(33.0);
            price12.setDateFrom(new SimpleDateFormat("yyyy-MM-dd").parse("2021-06-03"));
            price12.setDateTo(new SimpleDateFormat("yyyy-MM-dd").parse("2021-08-13"));
            priceRepository.save(price12);

            System.out.println(priceRepository.getCurrentPriceByProvider(provider1));
        };
    }
}
