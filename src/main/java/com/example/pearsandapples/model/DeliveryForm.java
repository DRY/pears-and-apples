package com.example.pearsandapples.model;

import lombok.Data;

@Data
public class DeliveryForm {
    private long providerId;
    private long priceId1;
    private double weight1;
    private long priceId2;
    private double weight2;
    private long priceId3;
    private double weight3;
    private long priceId4;
    private double weight4;


}
