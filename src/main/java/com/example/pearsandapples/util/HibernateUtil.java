package com.example.pearsandapples.util;

import com.example.pearsandapples.entity.*;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;

import java.util.Properties;

public class HibernateUtil {
    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory == null) {
            try {
                Properties properties = new Properties();

                properties.put(Environment.DRIVER, "org.h2.Driver");
                properties.put(Environment.URL, "jdbc:h2:mem:pears_apples");
                properties.put(Environment.USER, "sa");
                properties.put(Environment.PASS, "");
                properties.put(Environment.DIALECT, "org.hibernate.dialect.H2Dialect");

                properties.put(Environment.SHOW_SQL, "true");
                properties.put(Environment.CURRENT_SESSION_CONTEXT_CLASS, "thread");
                properties.put(Environment.HBM2DDL_AUTO, "create-drop");

                Configuration configuration = new Configuration();
                configuration.setProperties(properties);

                configuration.addAnnotatedClass(Delivery.class);
                configuration.addAnnotatedClass(DeliveryDetail.class);
                configuration.addAnnotatedClass(Price.class);
                configuration.addAnnotatedClass(Product.class);
                configuration.addAnnotatedClass(ProductType.class);
                configuration.addAnnotatedClass(Provider.class);

                ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                        .applySettings(configuration.getProperties()).build();

                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return sessionFactory;
    }
}
